import numpy as np

class KF_Lane:
    def __init__(self):
        self._numstates=2 # States

        self._dt = 1.0/20.0 # Sample Rate of the Measurements is 20Hz
        self._dtGPS=1.0/4.0 # Sample Rate of GPS is 4Hz

        self._P = np.diag([1.0, 1.0])

        sGPS    = 0.5*8.8*self._dtGPS**2  # assume 8.8m/s2 as maximum acceleration, forcing the vehicle

        self._Q = np.diag([sGPS**2, sGPS**2])

        varLane = 3.0 # Standard Deviation of GPS Measurement
        self._R = np.matrix([[varLane**2, 0.0],
                           [0.0, varLane**2]])

        self._I = np.eye(self._numstates)

    ## Initial State
    # The state x is [e_x, e_y]
    def init_state(self):
        self._x = np.matrix([[0.0,0.0]]).T

    @property
    def x(self):
        return self._x

    # Extended Kalman Filter
    def update(self, measurments):

        # Time Update (Prediction)
        # ========================

        # x_k+1 = x_k

        A = np.eye(self._numstates)
        H = np.eye(self._numstates)

        # Project the error covariance ahead
        self._P = A*self._P*A.T + self._Q

        # Measurement Update (Correction)
        # ===============================

        S = H*self._P*H.T + self._R
        K = (self._P*H.T) * np.linalg.inv(S)    

        # Update the estimate via the lane detection
        # the measurments Z is [d_x, d_y] = d_v - d_g; the difference between the result of lane detection from vision and the gps.
        Z = np.array([measurments]).T
        y = Z - (H*self._x)                         # Innovation or Residual    
        self._x = self._x + (K*y)

        # Update the error covariance
        self._P = (self._I - (K*H))*self._P

        return self._x
