import numpy as np

class EKF_CTRV:
    def __init__(self):
        numstates = 5 # States

        self._dt = 1.0/150.0 # Sample Rate of the Measurements is 100Hz
        self._dtGPS=1.0/4.0 # Sample Rate of GPS is 4Hz

        self._P = np.diag([1000.0, 1000.0, 1000.0, 1000.0, 1000.0])

        sGPS     = 0.5*8.8*self._dt**2  # assume 8.8m/s2 as maximum acceleration, forcing the vehicle
        sCourse  = 0.1*self._dt # assume 0.1rad/s as maximum turn rate for the vehicle
        sVelocity= 8.8*self._dt # assume 8.8m/s2 as maximum acceleration, forcing the vehicle
        sYaw     = 1.0*self._dt # assume 1.0rad/s2 as the maximum turn rate acceleration for the vehicle

        self._Q = np.diag([sGPS**2, sGPS**2, sCourse**2, sVelocity**2, sYaw**2])

        varGPS = 0.1 # Standard Deviation of GPS Measurement
        varspeed = 0.1 # Variance of the speed measurement
        varyaw = 0.05 # Variance of the yawrate measurement
        self._R = np.matrix([[varGPS**2, 0.0, 0.0, 0.0],
                           [0.0, varGPS**2, 0.0, 0.0],
                           [0.0, 0.0, varspeed**2, 0.0],
                           [0.0, 0.0, 0.0, varyaw**2]])

        self._I = np.eye(numstates)

    def set_R(self, varGPS):
        self._R[0,0] = varGPS**2 
        self._R[1,1] = varGPS**2

    ## Initial State
    # The state x is [x, y, heading(rad), speed(m/s), yawrate(rad/s)]
    def init_state(self, x):        
        self._x = np.array([x]).T
        x[3] += 0.001 # if speed is zero, Time Update is useless

        self._U=float(np.cos(x[2])*x[3])
        self._V=float(np.sin(x[2])*x[3])

    # Extended Kalman Filter
    def update(self, measurments=None,correction=False):
        # Time Update (Prediction)
        # ========================
        # Project the state ahead
        # see "Dynamic Matrix"

        if np.abs(measurments[4])<0.0001: # Driving straight
            self._x[0] = self._x[0] + self._x[3]*self._dt * np.cos(self._x[2])
            self._x[1] = self._x[1] + self._x[3]*self._dt * np.sin(self._x[2])
            self._x[2] = self._x[2]
            self._x[3] = self._x[3]
            self._x[4] = 0.0000001 # avoid numerical issues in Jacobians
        else: # otherwise
            self._x[0] = self._x[0] + (self._x[3]/self._x[4]) * (np.sin(self._x[4]*self._dt+self._x[2]) - np.sin(self._x[2]))
            self._x[1] = self._x[1] + (self._x[3]/self._x[4]) * (-np.cos(self._x[4]*self._dt+self._x[2])+ np.cos(self._x[2]))
            self._x[2] = (self._x[2] + self._x[4]*self._dt + np.pi) % (2.0*np.pi) - np.pi
            self._x[3] = self._x[3]
            self._x[4] = self._x[4]

        # Calculate the Jacobian of the Dynamic Matrix A
        # see "Calculate the Jacobian of the Dynamic Matrix with respect to the state vector"
        a13 = float((self._x[3]/self._x[4]) * (np.cos(self._x[4]*self._dt+self._x[2]) - np.cos(self._x[2])))
        a14 = float((1.0/self._x[4]) * (np.sin(self._x[4]*self._dt+self._x[2]) - np.sin(self._x[2])))
        a15 = float((self._dt*self._x[3]/self._x[4])*np.cos(self._x[4]*self._dt+self._x[2]) - (self._x[3]/self._x[4]**2)*(np.sin(self._x[4]*self._dt+self._x[2]) - np.sin(self._x[2])))
        a23 = float((self._x[3]/self._x[4]) * (np.sin(self._x[4]*self._dt+self._x[2]) - np.sin(self._x[2])))
        a24 = float((1.0/self._x[4]) * (-np.cos(self._x[4]*self._dt+self._x[2]) + np.cos(self._x[2])))
        a25 = float((self._dt*self._x[3]/self._x[4])*np.sin(self._x[4]*self._dt+self._x[2]) - (self._x[3]/self._x[4]**2)*(-np.cos(self._x[4]*self._dt+self._x[2]) + np.cos(self._x[2])))
        JA = np.matrix([[1.0, 0.0, a13, a14, a15],
                        [0.0, 1.0, a23, a24, a25],
                        [0.0, 0.0, 1.0, 0.0, self._dt],
                        [0.0, 0.0, 0.0, 1.0, 0.0],
                        [0.0, 0.0, 0.0, 0.0, 1.0]])


        # Project the error covariance ahead
        self._P = JA*self._P*JA.T + self._Q

        # Measurement Update (Correction)
        # ===============================
        # Measurement Function
        hx = np.matrix([[float(self._x[0])],
                        [float(self._x[1])],
                        [float(self._x[3])],
                        [float(self._x[4])]])

        if correction: # with 10Hz, every 5th step
            JH = np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0],
                            [0.0, 1.0, 0.0, 0.0, 0.0],
                            [0.0, 0.0, 0.0, 1.0, 0.0],
                            [0.0, 0.0, 0.0, 0.0, 1.0]])
        else: # every other step
            JH = np.matrix([[0.0, 0.0, 0.0, 0.0, 0.0],
                            [0.0, 0.0, 0.0, 0.0, 0.0],
                            [0.0, 0.0, 0.0, 1.0, 0.0],
                            [0.0, 0.0, 0.0, 0.0, 1.0]])

        S = JH*self._P*JH.T + self._R
        K = (self._P*JH.T) * np.linalg.inv(S)

        # Update the estimate via
        del measurments[2]
        Z = np.array([measurments]).T
        y = Z - (hx)                         # Innovation or Residual
        self._x = self._x + (K*y)

        # Update the error covariance
        self._P = (self._I - (K*JH))*self._P

        return self._x
