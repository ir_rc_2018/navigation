#!/usr/bin/env python

import numpy as np
import math
import copy

# ros package or messages
import rospy
import tf
from tf.transformations import quaternion_from_euler
from visualization_msgs.msg import Marker, MarkerArray
from nav_msgs.msg import Odometry
#from ublox_msgs.msg import NavVELNED
from std_msgs.msg import Int8 , Bool
from sensor_msgs.msg import NavSatFix, Imu, JointState
 #for ublox gps
from geometry_msgs.msg import Point, PoseWithCovarianceStamped, PoseStamped
from visualization_msgs.msg import Marker
from ublox_msgs.msg import NavVELNED, NavPOSECEF
from control_msgs.msg import VehicleState, GlobalPath
# reading data
from lib.utm import utm
from localization.sensor_fusion_positioning import SensorFusionPositioning

pub_gps = rospy.Publisher('/gps_pose', Odometry, queue_size=1)
pub_ekf = rospy.Publisher('/ekf_pose', Odometry, queue_size=1)
pub_vehilce_state = rospy.Publisher('/vehicle_state', VehicleState, queue_size=10)
pub_globalpath = rospy.Publisher('/global_path', GlobalPath, queue_size=1)
pub_object_global_path_points = rospy.Publisher('/marker/global_path_points', MarkerArray, queue_size=5)
pub_gps_enable = rospy.Publisher('/gps_enable', Bool, queue_size=5)


cur_pose = Odometry()
sensor_fusion_positioning = SensorFusionPositioning()

gps_enable = True
threshold_gps_enable = 120
pAcc_history = []
vehicle_state = VehicleState()

def callback_vehicle_speed(data):
    global vehicle_state

    wheel_R = 0.033 # [m]
    vehicle_state.v_ego = (data.velocity[0]+data.velocity[1])/(2*np.pi)*(2*np.pi*wheel_R)/2



def callback_imu(data):

    vehicle_state.yaw_rate = data.angular_velocity.z
    vehicle_state.steer_angle = 0.


    sensor_fusion_positioning.yawrate = vehicle_state.yaw_rate # [rad/s]
    sensor_fusion_positioning.speed = vehicle_state.v_ego # [m/s]

    if not sensor_fusion_positioning.is_initialize_pose():
        return


    if data.header.seq % 38 == 0 and gps_enable:
        sensor_fusion_positioning.update_pose(True, False)
    else:
        sensor_fusion_positioning.update_pose(False, False)

    draw_pose()

def callbackUblox(data):
    if sensor_fusion_positioning.gps_pose.heading == 0.0:
        return
    ublox_pose = utm.from_latlon(data.latitude, data.longitude)
    OFFSET_HD_MAP = np.array([353005.7222182057, 4026274.0666292924])
    sensor_fusion_positioning.gps_pose.x = ublox_pose[0]-OFFSET_HD_MAP[0]
    sensor_fusion_positioning.gps_pose.y = ublox_pose[1]-OFFSET_HD_MAP[1]

    # Initialize position and orientation from gps
    if not sensor_fusion_positioning.is_initialize_pose() \
        and sensor_fusion_positioning.speed > 0.001:
        sensor_fusion_positioning.initialize_pose()

def callbackNavVELNED(data):
#   if (data.speed*10**-2) > 2.7:

    sensor_fusion_positioning.gps_pose.heading = \
	-(3.141592/180)*(data.heading*0.00001 - 90)


def callbackNavPOSECEF(data):
    global pAcc_history, pub_gps_enable, gps_enable

    position_accuracy = data.pAcc

    if len(pAcc_history) < 20 :
        pAcc_history.append(position_accuracy)
    else :
        pAcc_history.pop(0)
        pAcc_history.append(position_accuracy)

    if len(pAcc_history) >= 20 and np.mean(pAcc_history) > threshold_gps_enable :
        pub_gps_enable.publish(False)
        gps_enable = False
    else :
        pub_gps_enable.publish(True)
        gps_enable = True

def callback_cur_position(msg):
    global cur_pose

    cur_pose.pose.pose.position.x = msg.pose.pose.position.x
    cur_pose.pose.pose.position.y = msg.pose.pose.position.y
    cur_pose.pose.pose.orientation = msg.pose.pose.orientation 

def callback_goal_position(goal_pose):
    global pub_globalpath, cur_pose, pub_object_global_path_points, vehicle_state

    #make path and publish
    start_point = [cur_pose.pose.pose.position.x , cur_pose.pose.pose.position.y]
    goal_point  = [goal_pose.pose.position.x , goal_pose.pose.position.y]
    
    start_point = [vehicle_state.x, vehicle_state.y]
    
    # goal_point  = [-378.916992188 , 304.244598389] # tennis court 
    goal_point  = [-520.531005859, -8.19647216797] # near building
    interval_path = 0.3 #[m]
    path_len = euclidean(start_point, goal_point)

    num_segment = int(np.floor(path_len/interval_path))
    len_last_segment = path_len/interval_path - num_segment

    unit_vector = np.subtract(goal_point,start_point)/path_len

    path_x = [start_point[0]]
    path_y = [start_point[1]]

    for idx in range(num_segment) :
        path_x.append( start_point[0] + (idx+1)*interval_path*unit_vector[0])
        path_y.append( start_point[1] + (idx+1)*interval_path*unit_vector[1])

    if len_last_segment > 0 :
        path_x.append(goal_point[0])
        path_y.append(goal_point[1])

    global_path = GlobalPath()
    global_path.path_x = path_x
    global_path.path_y = path_y
    pub_globalpath.publish(global_path)

    # marker
    global_path_points = MarkerArray()

    for idx in range(len(path_x)) :
    	marker = cylinder_visual_marker(path_x[idx],path_y[idx],[1,0,0],1,'global_path_point')
    	marker.id = idx
    	global_path_points.markers.append(marker)

    pub_object_global_path_points.publish(global_path_points)



def cylinder_visual_marker(x,y,color,tranparent_ratio,name_space) :
  
    marker = Marker()
    marker.id = 0
    marker.header.frame_id = '/map'
    marker.header.stamp = rospy.Time.now()
    marker.type = Marker.CYLINDER
    marker.action = Marker.ADD
    marker.ns = name_space

    marker.color.r = color[0]
    marker.color.g = color[1]
    marker.color.b = color[2]
    marker.color.a = tranparent_ratio

    marker.pose.position.x = x
    marker.pose.position.y = y

    q = quaternion_from_euler(0, 0, 0)

    marker.pose.orientation.x = q[0]
    marker.pose.orientation.y = q[1]
    marker.pose.orientation.z = q[2]
    marker.pose.orientation.w = q[3]

    marker.scale.x = .2
    marker.scale.y = .2
    marker.scale.z = 1

    marker.lifetime = rospy.Duration(1000)

    return marker


def euclidean(a,b) :
    return np.sqrt(np.sum(np.subtract(a,b)**2))




def draw_pose():
    global vehicle_state

    if not sensor_fusion_positioning.is_initialize_pose():
        return

    # for visualization
    pose = Odometry()
    pose.header.frame_id = '/map'
    pose.header.stamp = rospy.Time.now()
    pose.pose.pose.position.x = sensor_fusion_positioning.gps_pose.x
    pose.pose.pose.position.y = sensor_fusion_positioning.gps_pose.y
    quaternion = tf.transformations.quaternion_from_euler(0, 0, sensor_fusion_positioning.gps_pose.heading)
    pose.pose.pose.orientation.x = quaternion[0]
    pose.pose.pose.orientation.y = quaternion[1]
    pose.pose.pose.orientation.z = quaternion[2]
    pose.pose.pose.orientation.w = quaternion[3]

    pub_gps.publish(pose)

    pose.pose.pose.position.x = sensor_fusion_positioning.ekf_pose.x
    pose.pose.pose.position.y = sensor_fusion_positioning.ekf_pose.y
    quaternion = tf.transformations.quaternion_from_euler(0, 0, sensor_fusion_positioning.ekf_pose.heading)
    pose.pose.pose.orientation.x = quaternion[0]
    pose.pose.pose.orientation.y = quaternion[1]
    pose.pose.pose.orientation.z = quaternion[2]
    pose.pose.pose.orientation.w = quaternion[3]



    pub_ekf.publish(pose)
    cur_pose = copy.deepcopy(pose)
    vehicle_state.x = pose.pose.pose.position.x
    vehicle_state.y = pose.pose.pose.position.y
    vehicle_state.heading = sensor_fusion_positioning.ekf_pose.heading

    br = tf.TransformBroadcaster()
    br.sendTransform((sensor_fusion_positioning.gps_pose.x, sensor_fusion_positioning.gps_pose.y, 0),
                     tf.transformations.quaternion_from_euler(0, 0, sensor_fusion_positioning.gps_pose.heading),
                     rospy.Time.now(),
                     "odom",
                     "map")

def vehicle_state_timer(event) : 
    global vehicle_state, pub_vehilce_state
    data = copy.deepcopy(vehicle_state)
    pub_vehilce_state.publish(data)




if __name__ == '__main__':
    try:
      rospy.init_node('gps_visualization', anonymous=True)

      rospy.Subscriber("/joint_states", JointState, callback_vehicle_speed)
      rospy.Subscriber("/imu", Imu, callback_imu)
      rospy.Subscriber("/fix_ublox", NavSatFix, callbackUblox)
      rospy.Subscriber("/ublox/navvelned", NavVELNED, callbackNavVELNED)
      rospy.Subscriber("/ublox/navposecef", NavPOSECEF, callbackNavPOSECEF)

      rospy.Subscriber("/move_base_simple/goal", PoseStamped, callback_goal_position)
      rospy.Subscriber("/initialpose", PoseWithCovarianceStamped, callback_cur_position)
            # dt = 0.05
      #rospy.Timer(rospy.Duration(dt), draw_pose)

      _vehicle_state_timer      = rospy.Timer(rospy.Duration(0.1), vehicle_state_timer)


      rospy.spin()
      _vehicle_state_timer.shutdown()

    except rospy.ROSInterruptException:
      pass
