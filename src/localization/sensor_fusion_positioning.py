import numpy as np
import math
import copy

from lib.filtering.kalman.EKF_CTRV import EKF_CTRV


class Pose(object):
    def __init__(self, x, y, heading):
        self._x = x
        self._y = y
        self._heading = heading

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    @property
    def heading(self):
        return self._heading

    @heading.setter
    def heading(self, heading):
        self._heading = heading

class Measurements(object):
    def __init__(self):
        self._gps_pose = Pose(0.0, 0.0, 0.0)    # x, y, heading
        self._speed = 0.0                       # m/s
        self._yawrate = 0.0                     # rad/s

    @property
    def gps_pose(self):
        return self._gps_pose

    @gps_pose.setter
    def gps_pose(self, gps_pose):
        self._gps_pose = gps_pose



    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, speed):
        self._speed = speed

    @property
    def yawrate(self):
        return self._yawrate

    @yawrate.setter
    def yawrate(self, yawrate):
        self._yawrate = yawrate


class SensorFusionPositioning(Measurements):
    def __init__(self):
        #self._hd_map = HDMap()
        #self._hd_map.load_hd_map()

        self._gps_error = np.matrix([[0.0], [0.0]])
        self._ekf_pose = None

        self._ekf = EKF_CTRV()                  # for GPS/IMU data fusion

        self._global_path = None

        super(SensorFusionPositioning, self).__init__()

    @property
    def gps_error(self):
        return self.gps_error

    @property
    def ekf_pose(self):
        return self._ekf_pose

    @property
    def ekf(self):
        return self._ekf

    @property
    def global_path(self):
        return self._global_path

    @global_path.setter
    def global_path(self, global_path):
        self._global_path = global_path

    @property
    def gps_error(self):
        return self._gps_error


    

    @gps_error.setter
    def gps_error(self, gps_error):
        self._gps_error = gps_error

    def initialize_pose(self):
        if self._gps_pose == None:
            return

        self._ekf_pose = copy.deepcopy(self._gps_pose)
        self._ekf.init_state([self._gps_pose.x, self._gps_pose.y, self._gps_pose.heading, self._speed, self._yawrate])


    def is_initialize_pose(self):
        return self._ekf_pose != None

    def is_available_path(self):
        return True if self._global_path else False




    def update_pose(self, measured_gps, measured_lane):
        if measured_gps:
            if measured_lane:
                corrected_x = self._gps_pose.x - self._gps_error[1,0]*math.sin(self._gps_pose.heading)
                corrected_y = self._gps_pose.y + self._gps_error[1,0]*math.cos(self._gps_pose.heading)
                # corrected_pose = self.get_position_error_from_lane(self._gps_pose)
                # corrected_pose = Point(corrected_x, corrected_y)

                # print('corrected_x, y, error : ', corrected_x, corrected_y, self._gps_error[1], self._gps_error[1,0])

                self._ekf.update([corrected_x, corrected_y, self._gps_pose.heading, self._speed, self._yawrate],True)
            else:
                self._ekf.update([self._gps_pose.x, self._gps_pose.y, self._gps_pose.heading, self._speed, self._yawrate],True)
        else:
            self._ekf.update([0.0, 0.0, 0.0, self._speed, self._yawrate],False)

        self._ekf_pose.x = float(self._ekf._x[0])
        self._ekf_pose.y = float(self._ekf._x[1])
        self._ekf_pose.heading = float(self._ekf._x[2])




if __name__ =='__main__':

    a = SensorFusionPositioning()


    print('sensorfusionpositioning on')
